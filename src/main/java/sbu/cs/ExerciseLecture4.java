package sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        int ans = 1;
        for(int i=1;i<=n;i++){
            ans*=i;
        }
        return ans;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
   public long fibonacci(int n) {
       long last = 0, a = 1, c = 0;
       if (n == 0)
           return last;
       for (int i = 1; i < n; i++) {
           c = last + a;
           last = a;
           a = c;
       }
       return a;
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */

    public String reverse(String word) {
        int n = word.length();
        String rev = "";
        for(int i=n-1 ; i>=0 ; i--){
            rev = rev + word.charAt(i);
        }
        return rev;
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
        String line2 = line.replaceAll(" ", "").toLowerCase();
        String reversed = reverse(line2);
        if(reversed.equals(line2)){
            return true;
        }
        else {
            return false;
        }

    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {
        int n1 = str1.length();
        int n2 = str2.length();
        char[][] dot = new char[n1][n2];
        for(int i=0 ; i<n1 ; i++){
            for(int j=0 ; j<n2 ; j++){
                if(str1.charAt(i) == str2.charAt(j)){
                    dot[i][j] = '*';
                }
                else{
                    dot[i][j] = ' ';
                }
            }
        }
        return dot;
    }
}

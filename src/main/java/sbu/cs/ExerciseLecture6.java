package sbu.cs;

import java.util.ArrayList;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long ans = 0;
        for(int i=0 ; i<arr.length ; i+=2){
            ans += arr[i];
        }
        return ans;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int[] reversed = new int[arr.length];
        int len = arr.length;

        for(int i=0 ; i<arr.length ; i++){
            reversed[i] = arr[len-1];
            len-=1;
        }
        return reversed;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        int row1 = m1.length;
        int col1 = m1[0].length;
        int row2 = m2.length;
        int col2 = m2[0].length;
        if(col1 != row2){
            throw new RuntimeException();
        }
        double[][] ans = new double[row1][col2];

        for(int i = 0; i < row1; i++){
            for(int j = 0; j < col2; j++){
                for(int k = 0; k < row2; k++){
                    ans[i][j] = ans[i][j] + m1[i][k] * m2[k][j];
                }
            }
        }
        return ans;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        List<List<String>> listoflists = new ArrayList<>();

        int row = names.length;
        int column = names[0].length;

        for(int i=0 ; i<row ; i++)
        {
            List<String> list = new ArrayList<>();

            for(int j=0 ; j<column ; j++)
            {
                list.add(names[i][j]);
            }

            listoflists.add(list);
        }

        return listoflists;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        List<Integer> factlist = new ArrayList<>();

        for (int i = 2; i <= n; i++) {
            while (n % i == 0)
            {
                if(!factlist.contains(i)){
                    factlist.add(i);
                }
                n /= i;
            }
        }
        return factlist;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        line = line.replaceAll("[!,?$#%^&*()+=@]","");

        String[] words=line.split(" ");
        List<String> ans = new ArrayList<>();
        for(int i=0 ; i<words.length ; i++){
            ans.add(words[i]);
        }
        return ans;
    }
}

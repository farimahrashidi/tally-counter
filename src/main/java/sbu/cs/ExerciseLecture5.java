package sbu.cs;
import java.util.Random;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {
        Random random = new Random();
        String letters = "abcdefghijklmnopqrstuvwxyz";
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(letters.charAt(random.nextInt(letters.length())));
        }
        return sb.toString();
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        Random random = new Random();

        if(length < 3){
            throw new IllegalValueException();
        }

        int index1=0 , index2=0 , index3=0;

        if(length == 3){
            index1=1;
            index2=1;
            index3=1;
        }
        if(length!= 3){
            index1 = random.nextInt(length - 3) + 1;
            index2 = random.nextInt(length - index1 - 2) + 1;
            index3 = length - (index1+index2);
        }
        String letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String nums = "0123456789";
        String chars = "!@#$%*^";

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < index1; i++) {
            sb.append(letters.charAt(random.nextInt(letters.length())));
        }
        for (int i = 0; i < index2; i++) {
            sb.append(nums.charAt(random.nextInt(nums.length())));
        }
        for (int i = 0; i < index3; i++) {
            sb.append(chars.charAt(random.nextInt(chars.length())));
        }
        int sbLength = sb.length();
        StringBuilder pass = new StringBuilder(sbLength);
        int sbLength2 = sbLength;
        while(sbLength2 > 0){

            if(sbLength2 != 1){
                int rand = random.nextInt(sbLength2-1);
                pass.append(sb.charAt(rand));
                sb.deleteCharAt(rand);
                sbLength2 -= 1;
            }

            else if(sbLength2==1) {
                int rand = 0;
                pass.append(sb.charAt(rand));
                sbLength2 -= 1;
            }

        }


        return pass.toString();
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {
        int count = 0;
        for(int i=1 ; i<=n ; i++)
        {
            int fibo = fib(i);
            String strBin = Integer.toBinaryString(fibo);
            int intBin = Integer.parseInt(strBin);

            if(n == fibo + intBin){
                count+=1;
            }
        }
        if(count > 0){
            return true;
        }
        else{
            return false;
        }

    }
    public int fib(int n) {
        int last = 0, a = 1, c = 0;
        if (n == 0)
            return last;
        for (int i = 1; i < n; i++) {
            c = last + a;
            last = a;
            a = c;
        }
        return a;
    }
}
